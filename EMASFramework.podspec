Pod::Spec.new do |s|

  s.name         = "EMASFramework"
  s.version      = "1.0.0"
  s.summary      = "EMASFramework类库"

  s.description  = <<-DESC
                   * Detail about EMASFramework framework.
                   DESC

  s.homepage     = "http://"
  s.license      = 'MIT (example)'
  s.author       = { "未定义" => "undefined" }
  s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'
  s.source       = { :http => "http://EMASFramework.zip" }
  s.preserve_paths = "EMASFramework.framework/*"
  s.resources  = "EMASFramework.framework/*.{bundle,xcassets}"
  s.vendored_frameworks = 'EMASFramework.framework'
  s.requires_arc = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/EMASFramework' }

  #s.dependency 'XXXX'

end
